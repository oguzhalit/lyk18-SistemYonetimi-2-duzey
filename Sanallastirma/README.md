<html><head><meta charset="UTF-8"> </head><body> 
<h1>Virtualization (Sanallaştırma)</h1>
<p>Mesutcan Kurt hocamızın anlattığı sanallaştırma konusunu  aktarmaya çalışacağım.</p>
<p>Kendi notlarım ve hocamızın anlattıklarının aklımda kalanlarını anlatmayı planlıyorum. Herhangi bir konu da yanlış görürseniz lütfen bildirmeyi ihmal etmeyin.</p>
<ul>
<li>Derste centos veya debian dağıtımı sanal makine istenmektedir. Sınıfta genel olarak debian dağıtımı tercih edildiği için konu debian üzerinden anlatılmıştır. </li>
<li>Anlatımda ki hataları düzeltmek için: <a href="https://gitlab.com/rection">gitlab</a></li>
</ul>
<p>Sanallaştırma, fiziksel bir makinadan sanal kaynakların ayrılıp verimli halde kullanmayı sağlamaktadır. Sanallaştırma; bilgisayar donanımları, depolama cihazları ve bilgisayar ağları üzerinde yapılmektedir. Derste anlatılanlar disk, ağ ve <em>donanım</em> sanallaştırma olduğu için sadece bu konulardan bahsedilecektir.</p>
<h2>Ağ Sanallaştırma:</h2>
<p>Ağ sanallaştırması, bir sunucuda ki ağı bir çok alt ağa veya sanal bölümlere ayrılmasıdır. Genellikle yazılım testlerinde kullanılmaktadır. Yazılımın çalışması amaçlandığı ağ ortamlarının simülasyonunda kullanılmaktadır. Simülasyonun sunduğu seçenekler ise ağ kartı (switch ve ağ cihazı), firewall,load balancer, virtual LAN(sanal makinalarda kullanılması için), ağ depolama aygıtları, haberleşme aygıtlarının simülasyon yapılabilmektedir.</p>
<h2>Disk Sanallaştırma:</h2>
<p>Blok halinde veya dosya halinde sanallaştırma yapılmasını sağlamaktadır.  </p>
<p><em>Blok sanallaştırma</em>, diskte kurulmuş ama işlenmemiş bölümlerin kullanılması ve her bloğun bir birey gibi kontrol edilmesidir. Bu blokların kontrolü işletim sistemi tarafından yapılmaktadır. Her blok işletim sisteminin kendi formatını kullanmaktadır.  </p>
<p><em>Dosya sanallaştırma</em>, bir hard disk, NAS gibi depolama sistemleri için kullanılır. NFS veya SMB gibi protokoller kullanır. Depolama alanları sonrasında büyütülebilir. Daha fazla bilgi için <a href="https://stonefly.com/resources/what-is-file-level-storage-vs-block-level-storage">tıklayınız</a>.  </p>
<h2>RAID (Redundant Array of Independent Disks):</h2>
<p>Raid bir block sanallaştırma türüdür. Birden fazla diskle izin verilen input/output işlemlerinin iyileştirilmesini, dengelenmesini sağlamaktadır. Bu işlemler raid kontrol aracı (raid controller) ile olmaktadır. Raid kontrol aracı, işletim sistemi ve disklerle arasında bulunur. Mantıksal ünitelerin gruplandırılıp aktarılması sağlanmaktadır. Raidin seviyeleri bulunmaktadır. Her seviyenin kendine ait avantajı ve dezavantajı bulunmaktadır. Raid 0,1,3,5,7 hız ve güvenlik sağlamaktadır. Daha fazla bilgi için <a href="https://searchstorage.techtarget.com/definition/RAID">tıklayınız</a>.</p>
<h4>RAID 0:</h4>
<p>Sunucunun performansını iyileştirmek için kullanılır. Disk bölüştürme olarak bilinir. Kısaca bir yerden okuma/yazma yerine birden fazla yerden okuma/yazma yapabilmektedir. En az iki diske ihtiyaç duyar. Dezavantajı ise disklerden birisi bozulursa sıralama kayması nedeniyle veri kaybı yaşanır veya bozulmanın miktarı artar.</p>
<h4>RAID 1:</h4>
<p>Veri kaybını engellemesi için avantajlı bir sistemdir. İki diskten birisine yazarken diğer diske de kopyalanmaktadır. Yani diskin sürekli bir kopyası oluşturulmaktadır. Dezavantajı ise sürekli bir yedekleme olmasından dolayı performansın düşük olmasına sebep olmaktadır.</p>
<h4>RAID 3:</h4>
<p>Uzun süreli sıralı(sequential) veri akışlarında (medya yayıncılığı, video düzenlemeleri) kullanılır. Sıralı yazmasından dolayı küçük boyutlu veriler için performansızdır.</p>
<h4>RAID 5:</h4>
<p>Genellikle şirketlerin nas ve sunucularında kullanılır. RAID 1'den daha performanslı çalışır. RAID 5'te veri üç veya daha fazla diske ayrılır. Eğer disk bozulmaya başlarsa veri diğer disklerden otomatik ve sorunsuz bir şekilde tekrar gelir. Sistem çalışırken disk değiştirme yapılabilmektedir. Yeni disk takıldığı zaman otomatik olarak veriyi yaymaktadır. Dezavantajı ise fazla kullanıcı olduğunda performans sorunu yaşanmaktadır. </p>
<h4>RAID 7:</h4>
<p>RAID 3 ve RAID 4'ün özelliklerini bulundurmaktadır. Ek olarak saklama (cache) özelliği vardır.</p>
<h2>Donanım Sanallaştırma:</h2>
<p><img alt="Alt text" src="Sanallastirma/eklenti/SON1.png" />  </p>
<h3>Full Virtualization:</h3>
<p>Donanımın bütünüyle simülasyonlaştırılmasıdır. Yazılıma donanımın parçalarınından birini farklı binary kodlaması kullanılmasına izin vermesidir.</p>
<h3>Paravirtualization:</h3>
<p>Bir donanımın simülasyon edilmesi yerine programın izole edilmiş bir ortamda çalıştırılmasıdır.  </p>
<h3>Hardware-assisted Virtualization:</h3>
<p>Donanımda sanallaştırma için destek sağlayan CPU'ları ve performansını iyileştirmeye yardımcı olan diğer donanım bileşenlerini içerir. Bu sayede donanımı daha verimli kullanarak daha performanslı çalışmasını sağlar.</p>
<p><img alt="Alt text" src="Sanallastirma/eklenti/Virtualization.png" /></p>
<h3>Wine</h3>
<p>Windows tarafından geliştirilmiş yazılım ve oyunların Unix türevli sistemlerde çalıştırılması için bulunan özgür ve açık kaynak bir platformdur. Windows üzerinde çalışan programın çalışmasında kullanılan yazılım kütüphanelerini Unix türevli bilgisayarlarda uyumlu olmasını sağlamaktadır. Bu kütüphaneler sayesinde donanım üzerinden sanallaştırma yapılmasını sağlar. Wine is not emulator tanımı ise wine'nın emülatör olmadığını söylemektedir. Wine birleşik katmanlı yapıda çalışabilen,  POSIX standartlarına sahip işletim sistemlerinde çalışan windows uygulamalarını çalıştırmaya yarayan bir araçtır. Windowsta ayrı bir sanal makina veya emulatör simüle yapılması yerine wine windows'ta  uygulamayı POSIX'in içinden kullanmaktadır.</p>
<p>Ayrıca sanallaştırma teknolojisini kullanabilmek için işlemciler üzerinde sanallaştırma seçeneği açık olmalıdır. İşlemcilerin modeline göre değişmektedir. Bios ayarı üzerinden sanallaştırma katmanı kullanmak için aktifleştirilmesi gerekmektedir.</p>
<h3>Long Mode</h3>
<p>Diğer bir deyişle bilgisayar mimarisinde bulunan x86-64 dizaynıdır. long mode, 64 bit işletim sistemlerinde 64 bitlik işlemler ve registerları kullanabilmesini sağlar. 64 bitlik programlar 64 bitlik modda çalışır, 32 veya 16 bitlik programlarda koruma moduna(<a href="https://en.wikipedia.org/wiki/Protected_mode">Protected Mode</a>) geçilip çalışması sağlanmaktadır.</p>
<h3>Docker</h3>
<p>Docker'dan bahsetmeden önce konteyner teknolojisinden bahsedilmesi gerekmektedir. Konteyner, 1960 ve 1970 seneleri arasında <a href="https://en.wikipedia.org/wiki/Chroot">chroot</a> işlemlerinin gelişmesiyle ortaya çıkmıştır. chroot işlemlerinin şu andaki ismi Docker ve sistem konteynerlaştırmadır. Şu anki ismi ise (<a href="https://en.wikipedia.org/wiki/LXC">LXC</a>)'dir. Konteyner imajları çalışan işletim sistemleri üzerinde bir konteyner motoru(container engine) olmalıdır. Konteynerlaştırılmış uygulamalar bir çok konteynır imajı olarak oluşturulabilir ve konteynırlar birleşik yapıda çalışabilir.</p>
<p>Docker konteynerları ise sanallaştırılmış uygulama konteynerlarını işletim sistemleri üzerinden yönetmek için kullanılmaktadır. Bir docker konteyner, uygulama servisini veya fonksiyonların bütün kütüphanleri ile beraber, ayar dosyaları, bağımlılıkları ve diğer operasyon parçalarını bulundurmaktadır. Her konteyner işletim sisteminin servisi(process) aracılığıyla yönetilebilmektedir.</p>
<p><img alt="Alt Text" src="Sanallastirma/ENSON2.png" /></p>
<p>Docker'ın sanallaştırmadan en önemli farkı; sanallaştırma, sanallaştırma katmanın üzerinde bulunan bir işletim sisteminin yönetimini sağlamaktaydı. Docker ise sanallaştırma katmanı bulunmadan docker engine sayesinde üzerinde uygulama çalıştırmaya yarar.</p>
<h3>KVM (Kernel-based Virtual Machine)</h3>
<p>Full-virtualization kullanarak linux kerneli sanallaştırma aracıyla(Hypervisor) sanal makina kurmaktadır.</p>
<p>Derste yapılan uygulamada genel olarak debian kullandık. Bu yüzden kodlar debian için geçerlidir.</p>
<p><code>sudo apt-get install qemu-kvm</code></p>
<p>qemu-kvm'i yüklemesini sağlar. Centos dağıtımında ismi değişmektedir.</p>
<p><code>qemu-img create test.img 500M</code></p>
<p>qemu-kvm aracılığıyla sanallaştırma yapılacak olan işletim sisteminin alanı oluşturulmaktadır. 'test.img' kısmında imaj dosyasının ismi verilmektedir. '500M' ise boyutu en fazla 500 megabayt olarak ayarlanmasını sağlar. İmaj dosyasının burada ki amacı okuma/yazma işlemlerini yapmasıdır. qemu-kvm'in kullandığı dosya formatı <a href="https://people.gnome.org/~markmc/qcow-image-format.html">qcow2</a>'dir</p>
<p><code>qemu-system-x86_64 -m 1000 -boot d -cdrom test.img</code></p>
<p>Debian dağıtımlarında farklılık gösterebilir. Benim kullanıdığım '<em>Debian 3.16.56-1+deb8u1 x86_64 GNU/Linux</em>' da qemu-system-x86_64 programı kullanarak imaj dosyamızı sanallaştırarak ayrı bir işletim sistemi sunmamızı sağlar. '<em>-m 1000</em>' sistem tarafından ayrılması istenen bellek miktarını belirtmektedir. '<em>-boot d</em>' boot parametresi boot aşamasının nasıl olacağını ve sonrasında görüntünün nasıl olacağını belirlenmesini sağlar. Burada ki 'd' ilk olarak cdromdan başlatılmasını söylemektedir. '<em>-cdrom test.img</em>' imaj olarak '<em>test.img</em>' kullanılmasını söylemektedir. Sonrasında okuma/yazma işlemleri test.img üzerine yapılmaktadır.  </p>
<p>Eğer bir arayüz üzerinden kontrol etmek isterseniz '<em>virt-manager</em>' adında paketi yükleyebilirsiniz. Buna ek olarak virsh adında bir kvm yönetim aracı da vardır. Virsh komut satırı üzerinden yönetimini kolaylaştırmaktır. Eğer yüklemek isterseniz <a href="https://wiki.debian.org/KVM#Installation">bağlantıyı</a> takip edebilirsiniz.</p>
<p>Ders'te anlatılan konular bu kadardı. Eklemek istediğiniz bir bilgi olursa <a href="https://gitlab.com/rection">gitlab</a> hesabıma yazabilirsiniz.</p></body>
</html>
